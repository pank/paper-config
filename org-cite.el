;; Use citations on form [[key: common pre; pre1 @k1 post1; \cdots; preN @kN postN; common post]]
;; short citations supported: @key -> textcite; (@key) -> parencite.

(require 'ox)
(require 'ox-latex)
(require 'bibtex)
(require 'reftex-cite)
;; (defmacro pank/org-cite-add-links (type &optional follow)
;;   "Create many cite links at once."
;;   `(org-add-link-type
;;     ,type
;;     ,(lambda (arg) (let ((fun (if follow follow 'identity)))
;;                 (funcall fun arg)))
;;     ,(lambda (path description backend)
;;        ;; [[cite: ]] is shorthand for textcite
;;        (funcall 'pank/org-cite-format path description backend
;;                 (if (equal type "cite") "textcite" type)))))

;; TODO: funcall on the macro doesn't work in Emacs 24.4 for some reason.
;; Enable cite links.
;; (mapc (lambda (type) (funcall 'pank/org-cite-add-links type))
;;       '("cite" "textcite" "parencite" "citeyear" "citeauthor"))

(org-add-link-type "cite" 'identity
                   (lambda (path description backend)
                     (pank/org-cite-format path description backend "textcite")))

(org-add-link-type "textcite" 'identity
                   (lambda (path description backend)
                     (pank/org-cite-format path description backend "textcite")))

(mapc (lambda (key)
        (org-add-link-type key 'identity
                           (lambda (path description backend)
                             (pank/org-cite-format path description backend "parencite"))))
      '("(cite)" "parencite"))


;; (org-add-link-type "parencite" 'identity
;;                    (lambda (path description backend)
;;                      (pank/org-cite-format path description backend "parencite")))

(org-add-link-type "citeauthor" 'identity
                   (lambda (path description backend)
                     (pank/org-cite-format path description backend "citeauthor")))

(org-add-link-type "citeyear" 'identity
                   (lambda (path description backend)
                     (pank/org-cite-format path description backend "citeyear")))


(add-to-list 'bibtex-files "lit.bib")

(defconst pank/org-cite-re
  "\\`\\([^@]*\\|\\([^@]*\\)@\\([^ \t]*\\)\\([^@]*\\)\\)\\'"
  "Slot 1: *common* pre- or post-note.
Slot 2: pre-note.
Slot 3: key.
Slot 4: post note.")

(defun pank/org-cite-interpret-string (string)
  "Interpret at string according to `pank/org-cite-re'"
  (mapcar
   (lambda (data)
     (string-match pank/org-cite-re data)
     (list :key  (org-string-nw-p (match-string 3 data))
	   :pre  (org-trim (or (match-string 2 data)
			       (match-string 1 data) ""))
	   :post (org-trim (or (match-string 4 data) ""))))
   (org-split-string (org-link-unescape string) ";")))

(defun pank/org-cite-extract-common-notes (n)
  "Extract common note from cell N if present and delete cell N"
  ;; TODO: not lexical-scope proof.
  (let ((first (nth n citations)))
    (org-trim
     (or (unless (plist-get first :key)
           (prog1 (format "(%s)" (plist-get first :pre))
             (setf (nth n citations) nil)))
         ""))))


(defun pank/format-plaintext-textcite (cite)
  (let ((post (org-string-nw-p (plist-get cite :post))))
    (format "%s (%s%s%s)"
            (plist-get cite :author)
            (org-trim (plist-get cite :pre))
            (plist-get cite :year)
            (if post (format ", %s" (org-trim post)) ""))))

(defun pank/format-plaintext (cite &optional no-year no-author)
  (let ((post (org-string-nw-p (plist-get cite :post)))
        (pre (org-string-nw-p (plist-get  cite  :pre))))
    (format "%s%s %s%s"
            (if pre (concat (org-trim (plist-get cite :pre)) " ") "")
            (if no-author "" (plist-get cite :author))
            (if no-year "" (plist-get cite :year))
            (if post (concat " " post) ""))))

(defun pank/org-cite-format (path description backend type)
  "Interpret a citation link and return a latex citation."
  (let* ((citations (pank/org-cite-interpret-string path))
         (common-pre (pank/org-cite-extract-common-notes 0) )
         (common-post (pank/org-cite-extract-common-notes
                       (1- (length citations)))))
    (if (org-export-derived-backend-p backend 'latex)
        (format "\\%s%s%s%s%s"
                type (if (> (length citations) 1) "s" "")
                common-pre common-post
                (mapconcat
                 (lambda (cite)
                   (let ((pre (org-string-nw-p (plist-get cite :pre)))
                         (post (org-string-nw-p (plist-get cite :post))))
                     ;; TODO: probably format-spec would be good here...
                     (concat (cond
                              ((and pre post)
                               (format "[%s][%s]" pre post))
                              (post (format "[%s]" post))
                              (pre (format "[%s][]" pre))
                              (t ""))
                             (format "{%s}" (plist-get cite :key)))))
                 (delq nil citations) ""))
      ;; TODO, no support for global pre and post ATM
      (let ((textcitations
             (mapcar
              (lambda (cite)
                (let ((entry (progn (bibtex-search-entry (plist-get cite :key) t 0)
                                    (bibtex-parse-entry)))
                      (reftex-cite-punctuation '(", " " and " " et al.")))
                  (if entry
                      (list :author (or (org-remove-double-quotes
                                         (reftex-format-citation entry "%2a")) "")
                            :year (org-remove-double-quotes
                                   (reftex-format-citation entry "%y"))
                            :pre (plist-get cite :pre)
                            :post (plist-get cite :post))
                    (error "failed to find %s" (plist-get cite :key)))))
              (delq nil citations))))
        (if (equal type "parencite")
            (format "(%s%s%s)" (or common-pre "")
                    (mapconcat 'pank/format-plaintext textcitations "; ")
                    (or common-post ""))
          (format "%s%s%s" common-pre
                  (cl-case (intern type)
                    (parencite)
                    (citeauthor (mapconcat (lambda (cite) (pank/format-plaintext cite t)) textcitations ", "))
                    (citeyear (mapconcat (lambda (cite) (pank/format-plaintext cite nil t)) textcitations ", "))
                    (textcite (let ((str (mapconcat 'pank/format-plaintext-textcite textcitations "| "))
                                    (x 0) (start 0))
                                (while (string-match "| " str start)
                                  (setq start (match-end 0))
                                  (incf x))
                                (replace-regexp-in-string "| " ", "
                                                          (cond ((> x 1) (replace-match ", and " t nil str))
                                                                ((eq x 1) (replace-match " and " t nil str))
                                                                (t str))))))
                  common-post))))))

(defconst pank/org-cite-short-re "\\(^\\|[ ]\\|(\\)\\<\\(@[^@\\[{}\\.,?]+?\\>\\))?$?"
  ;; "\\(^\\|[ ]\\|(\\)\\<\\(@[^@ ]+?\\>\\))?$?"
  ;;"\\(^\\|[ ]\\|(\\)\\<\\(@[^@ ]+?\\>$\\))?" 
  ;;"\\((?\\)\\<\\(@[^@ \\t]+\\)\\>\\()?\\)"
  "slot 1: contains an opening parentheses
slot 2: contains the key
slot 3: contains a closing parentheses.")

(defconst pank/org-cite-short-highlight-re "(?\\<@[^@\\[{}\\.,? \n\t]+\\>)?$?"
  ;; "(?\\<@[^@ ]+\\>)?$?"
  "Regexp to use for highlighting short citations in the buffer")

(defcustom pank/org-cite-allow-short-keys t
  "Should short keys matching `pank/org-cite-short-re' be
  translated to proper citaitons?"
  :type 'boolean)

(defun pank/org-cite-highlight-short-links ()
  "Highlight short links if `pank/org-cite-allow-short-keys' is non-nil."
  (interactive)
  (and pank/org-cite-allow-short-keys
       (highlight-regexp pank/org-cite-short-highlight-re 'org-link)))

(defun pank/org-cite-short-citation-hooks (backend)
  "Translate @KEY and (@KEY) to textcite and parentcite"
  ;; TODO: pretty hard-coded...
  (switch-to-buffer (current-buffer))
  (and pank/org-cite-allow-short-keys
       (while (search-forward-regexp pank/org-cite-short-re nil t)
         (unless (save-match-data
                   (memq (org-element-type (org-element-context))
                         '(link fixed-width src-block)))
           (replace-match (format " [[%s:%s]]"
                                  (if (org-string-nw-p (match-string 1))
                                      "(cite)" "cite")
                                  (match-string 2)))))))

(add-hook 'org-export-before-parsing-hook
          'pank/org-cite-short-citation-hooks)

(add-hook 'org-mode-hook
          'pank/org-cite-highlight-short-links)

(provide 'org-cite)
