;;* dependencies
;; load packages, might contain newer version of org
(package-initialize)
(require 'cl)
(require 'org)
(require 'org-table)
(require 'ox)
(require 'ox-latex)
(require 'ox-odt)

(with-eval-after-load 'ox-odt
  (let ((schema-dir "/usr/share/emacs/etc/org/schema/")
        (style-file "/usr/share/emacs/etc/org/styles/OrgOdtStyles.xml")
        (template-file "/usr/share/emacs/etc/org/styles/OrgOdtContentTemplate.xml"))
    (setq org-odt-schema-dir schema-dir)
    (setq org-odt-styles-file style-file)
    (setq org-odt-content-template-file template-file)))

(mapc (lambda (file) (when (file-exists-p file) (load-file file))
        (when (file-exists-p (concat "paper-config/" file))
          (load-file (concat "paper-config/" file))))
      (append (list "org-cite.el")
              (unless (featurep 'ox-koma-letter)
                (list "ox-koma-letter.el"))))
(require 'ox-koma-letter)
(require 'org-inlinetask)


;;* export options
(setq org-export-date-timestamp-format "%B %e, %Y"
      org-export-allow-bind-keywords t
      org-latex-tables-booktabs t
      org-latex-tables-centered t
      org-latex-image-default-width nil
      org-export-babel-evaluate t
      org-latex-listings 'minted
      org-export-with-tags t
      org-export-with-inlinetasks t
      org-export-headline-levels 5
      org-export-with-statistics-cookies nil
      org-export-with-smart-quotes t
      org-export-with-toc nil
      org-latex-remove-logfiles t
      org-export-with-todo-keywords t
      org-latex-compiler "xelatex"
      org-latex-caption-above '(table special-block))

(setq org-confirm-babel-evaluate nil
      org-latex-default-class "koma-article")

;;* export process 
;; TODO: This can be rewritten using the latex_program declaration.
(setq org-latex-pdf-process
      (cond
       ((and (executable-find "latexmk") (executable-find "xelatex"))
        '("latexmk -g -pdf -pdflatex=\"%latex -8bit --shell-escape\" -outdir=%o %f"))
       ((executable-find "xelatex")
        '("xelatex -interaction nonstopmode -output-directory %o %f"
          "bibtex %b"
          "xelatex -interaction nonstopmode -output-directory %o %f"
          "xelatex -interaction nonstopmode -output-directory %o %f"))
       ((executable-find "latexmk")
        '("latexmk -g -pdf %f"))
       ((executable-find "pdflatex")
        '("pdflatex -interaction nonstopmode -output-directory %o %f"
          "bibtex %b"
          "pdflatex -interaction nonstopmode -output-directory %o %f"
          "pdflatex -interaction nonstopmode -output-directory %o %f"))
       (t (error "no latex compiler found"))))

;; sort out issue with time stamps

(setq system-time-locale "C")
;;* preamble.

;; TODO: Maybe some of this should be inserted as #+latex_header and
;; #+beamer_hearder instead...
(setq org-latex-default-packages-alist
      '(("" "iftex" )
        ;; Not in beamer
        ("" "scrpage2" )
        ("auto" "inputenc" t ("pdflatex"))
        ("T1" "fontenc" t ("pdflatex"))
        ("AUTO" "babel" t ("pdflatex"))
        ("AUTO" "polyglossia" nil ("xelatex" "lualatex"))
        ("" "graphicx" t)
        "% Allow better usage of page when lots of floats
\\renewcommand{\\bottomfraction}{0.8}
\\renewcommand{\\topfraction}{0.9}"
        ("" "placeins")
        ("" "amsmath" t)
        ("" "amssymb" t)
        ("" "amsthm" t)
        "\\theoremstyle{plain}
\\newtheorem{hypothesis}{Hypothesis}
\\theoremstyle{definition}
\\newtheorem{proposition}{Proposition}
"
        ("" "mathtools" t)
        ("" "booktabs" t)
        "% always center tables
 \\addto\\table{\\centering}"
        ("" "textcomp")
        ("osf, ttscale=.8" "libertine" t ("pdflatex"))
        ("libertine" "newtxmath" t ("pdflatex"))
        "% Font configuration.
\\makeatletter
\\ifPDFTeX\\else%
\\@ifclassloaded{beamer}{% Beamer
\\usepackage[sfdefault,scaled=.85]{FiraSans}
\\usepackage{newtxsf}%
}{% normal latex
\\usepackage{fontspec}
\\setmainfont{libertinusserif}[
Path = paper-config/libertinus/,
Extension = .otf,
UprightFont = *-regular,
BoldFont = *-semibold,
ItalicFont = *-italic,
BoldItalicFont = *-semibolditalic,
Numbers=OldStyle]
\\setsansfont{libertinussans}[
Path = paper-config/libertinus/,
Extension = .otf,
UprightFont = *-regular,
BoldFont = *-bold,
ItalicFont = *-italic,
Scale = MatchLowercase,
Numbers=OldStyle]
\\setmonofont{libertinusmono-regular.otf}[
Path = paper-config/libertinus/,
Numbers=OldStyle,
Scale = MatchLowercase]
\\usepackage[mathrm=sym, mathit=sym, mathsf=sym, mathbf=sym, mathtt=sym, math-style=ISO]{unicode-math}%
\\AtBeginDocument{\\renewcommand\\checkmark{\\usefont{U}{msa}{m}{n}X}}%
\\setmathfont[Scale=MatchLowercase, AutoFakeBold, Path = paper-config/]{libertinusmath.otf}}
% prefer osf figures...
% ... even osf figures even i math
% \\AtBeginDocument{
% \\DeclareMathSymbol{0}{\\mathord}{letters}{`0}
% \\DeclareMathSymbol{1}{\\mathord}{letters}{`1}
% \\DeclareMathSymbol{2}{\\mathord}{letters}{`2}
% \\DeclareMathSymbol{3}{\\mathord}{letters}{`3}
% \\DeclareMathSymbol{4}{\\mathord}{letters}{`4}
% \\DeclareMathSymbol{5}{\\mathord}{letters}{`5}
% \\DeclareMathSymbol{6}{\\mathord}{letters}{`6}
% \\DeclareMathSymbol{7}{\\mathord}{letters}{`7}
% \\DeclareMathSymbol{8}{\\mathord}{letters}{`8}
% \\DeclareMathSymbol{9}{\\mathord}{letters}{`9}
% }
\\fi
\\makeatother
"
        ("" "microtype")
        ("" "slantsc" )
        ("" "capt-of")
        ("" "subcaption")
        "% Enumitem
\\makeatletter
\\@ifclassloaded{beamer}{}{%
\\usepackage[inline, shortlabels]{enumitem}%
\\newlist{iand}{enumerate*}{1}%
\\setlist*[iand]{label=(\\roman*), ref=(\\roman*), before=\\unskip{: },%
  itemjoin={{;   }}, itemjoin*={{, and }}}%
%
\\newlist{ior}{enumerate*}{1}%
\\setlist*[ior]{label=(\\roman*), ref=(\\roman*),  before=\\unskip{: },%
    itemjoin={ {; }}, itemjoin*={{, or }}}}%
\\makeatother
"
        ("authordate, natbib, backend=biber,ibidtracker=false,
maxcitenames=2, citetracker=true" "biblatex-chicago" )
        ("" "tikz" )
        "% Math options.
\\mathtoolsset{showonlyrefs,showmanualtags}
\\newcommand{\\indicator}[1]{ \\ensuremath{\\ifPDFTeX\\mathbf{1}\\else\\mathbb{1}\\fi\\left\\{#1\\right\\}}}
\\DeclareMathOperator{\\E}{E}
\\newcommand{\\suchthat}{\\nonscript\\;\\ifnum\\currentgrouptype=16 \\middle\\fi|\\nonscript\\;}
"
        "% Captions and section look, only in KOMA-script.
\\makeatletter
\\@ifundefined{KOMAClassName}{}{%
\\@ifclassloaded{scrlttr2}{}{%
\\addtokomafont{captionlabel}{\\small\\sffamily}
\\addtokomafont{caption}{\\small\\sffamily}
\\setcapindent{1em}

\\renewcommand*{\\othersectionlevelsformat}[3]{%
  \\makebox[0pt][r]{{#3}\\autodot\\enskip}}}}
\\makeatother
"
        "% Footnote and itemize format, only in KOMA-script.
\\makeatletter
\\@ifundefined{KOMAClassName}{}{%
\\deffootnote{1em}{1em}{\\makebox[1em][l]{\\thefootnotemark}}}
\\makeatother
"
        "% Labels.
\\makeatletter
\\@ifundefined{labelitemi}{}{
\\renewcommand\\labelitemi{\\normalfont\\bfseries\\textendash}
\\renewcommand\\labelitemii{\\normalfont\\bfseries\\textbullet}}
\\makeatother
"
        "% Bibliographies.
\\addbibresource{lit.bib}
"
        ;; Not compatible with beamer.  Fix via filer.
        ;; Why not default beamer settings or some hypersetup?
        ("unicode, psdextra,hidelinks" "hyperref" )))

(add-to-list 'org-latex-classes
             '("koma-article"
               "\\documentclass[fontsize=11pt,
captions=tableheading,
parskip=half*]{scrartcl}
[PACKAGES]
[DEFAULT-PACKAGES]
[EXTRA]
"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\minisec{%s}" . "\\minisec{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))


(add-to-list 'org-latex-classes
             '("koma-letter"
               "\\documentclass[fontsize=11pt,
paper=a4,
foldmarks=TBmpl, 
enlargefirstpage=true,
refline=narrow,
parskip=half*]{scrlttr2}
\\usepackage{url}\\urlstyle{rm}
\\setkomafont{subject}{\\mdseries\\bfseries}
\\setkomafont{backaddress}{\\rmfamily}
\\setkomavar{firsthead}{}"))

;;* links
(autoload 'search "cl")

(org-add-link-type
 "latex" nil
 (lambda (path desc format)
   (cond
    ((eq format 'html)
     (format "<span class=\"%s\">%s</span>" path desc))
    ((org-export-derived-backend-p format 'latex)
     (format "\\%s{%s}" path desc)))))

(org-add-link-type
 "sc" nil
 (lambda (path desc format)
   (let ((txt (or desc path)))
     (cond
      ((eq format 'html)
       (format "<span class=\"small-caps\">%s</span>" txt))
      ((org-export-derived-backend-p format 'latex)
       (format "\\textsc{%s}"  txt))
      (t (upcase txt))))))


;;* Filters

(defun pank/org-beamer-filter-remove-hyperref (text backend info)
  "Remove hyperref from beamer tex files.

My default values are incompatible with beamer."
  ;; TODO: make better solution; check if still necessary
  (when (org-export-derived-backend-p backend 'beamer)
    (replace-regexp-in-string
     "\\\\usepackage\\[\\(.*?\\)\\]{hyperref}\n"
     ""
     text)))

(add-to-list 'org-export-filter-final-output-functions
             'pank/org-beamer-filter-remove-hyperref)

(defun pank/org-latex-filter-nobreaks-double-space (body backend info)
  "Tries to export \"S1. S2\" as \"S1.\\ S2\",
   while letting \"S1.  S2\" be exported without tilde"
  ;; TODO: error with this output:
  ;; [[file:nasty dir/Screenshot. from 2015-03-05 19:05:00.png]]
  (when (and body (org-export-derived-backend-p backend 'latex))
    (let ((case-fold-search nil))
      (replace-regexp-in-string "\\. \\([^ A-Z\n]\\)" ".\\\\ \\1" body))))

(add-to-list 'org-export-filter-body-functions
             'pank/org-latex-filter-nobreaks-double-space)

(defun pank/org-guess-textsc (content backend info)
  "Automatically downcase and wrap all-caps words in textsc.
The function is a bit slow...

TODO: Make the function work with headlines, but without doing it
on subsequent text.

TODO: Add ODT support."
  (if (org-export-derived-backend-p backend 'latex 'html)
      (let* (case-fold-search
             (latexp (org-export-derived-backend-p backend 'latex))
             (wrap (if latexp "\\textsc{%s}"
                     "<span class=\"small-caps\">%s</span>")))
        (replace-regexp-in-string
         "\\w+"
         (lambda (str)
           (if (or (string-equal str (downcase str))
                   (string-equal str (capitalize str)))
               str
             (replace-regexp-in-string
              "[[:upper:]]+"
              (lambda (x) (format wrap (downcase x)))
              str t t)))
         content t t))
    content))

(add-to-list 'org-export-filter-plain-text-functions
             'pank/org-guess-textsc)


(defun pank/org-latex-itemize-remove-relax (string backend info)
  "Get rid of annoying \\relax macros in LaTeX itemize-like environment."
  (replace-regexp-in-string
   (if (org-export-derived-backend-p backend 'latex)
       "\\(\\\\item\\) ?\\\\relax" "") "\\1" string))

(add-to-list 'org-export-filter-item-functions
             'pank/org-latex-itemize-remove-relax)


(defun pank/org-latex-eqref (content backend info)
  (when (and (org-export-derived-backend-p backend 'latex )
             (string-match-p "\\usepackage{mathtools}" content))
    (replace-regexp-in-string "\\\\ref{eq:" "\\\\eqref{eq:" content t )))

(add-to-list 'org-export-filter-final-output-functions
             'pank/org-latex-eqref)

(defun pank/org-latex-footnotes-in-headers (headline backend info)
  (when (and (org-export-derived-backend-p backend 'latex ))
    (with-temp-buffer
      (save-excursion (insert headline))
      (let ((eol (line-end-position)))
        (while (and (< (point) eol) (search-forward "\\footnote" eol t))
            (replace-match "\\protect\\footnote" t t)))
      (buffer-string))))

(add-to-list 'org-export-filter-headline-functions
             'pank/org-latex-footnotes-in-headers)


;; I think org-element-insert-before is only added in 8.3.
(when (fboundp 'org-element-insert-before)
  (defun org-export-ignore-headlines (data backend info)
    "Remove headlines tagged \"ignoreheading\" retaining contents and promoting children.
Each headline tagged \"ignoreheading\" will be removed retaining its
contents and promoting any children headlines to the level of the
parent."
    (org-element-map data 'headline
      (lambda (head)
        (when (or (member "ignoreheading" (org-element-property :tags head))
                  (org-not-nil (org-element-property :NOHEADING head)))
          (let ((level-top (org-element-property :level head))
                level-diff)
            (mapc (lambda (el)
                    ;; Recursively promote all nested headlines.
                    (org-element-map el 'headline
                      (lambda (el)
                        (when (equal 'headline (org-element-type el))
                          (unless level-diff
                            (setq level-diff (- (org-element-property :level el)
                                                level-top)))
                          (org-element-put-property
                           el
                           :level (- (org-element-property :level el)
                                     level-diff)))))
                    ;; Insert back into parse tree.
                    (org-element-insert-before el head))
                  (org-element-contents head)))
          (org-element-extract-element head)))
      info nil)
    data)

  (add-hook 'org-export-filter-parse-tree-functions
            'org-export-ignore-headlines))

(defun pank/org-latex-wrap-figures-in-makebox (text backend info)
  "Insert all figures in makeboxes for centering"
  (when (org-export-derived-backend-p backend 'latex)
    (let ((resize-boxes-re "\\\\resizebox{\\([^}]+\\)}{!?}{\\(.+?\\)}"))
      (with-temp-buffer
        (save-excursion
          (insert (replace-regexp-in-string resize-boxes-re "\\\\makebox[\\1]{\\2}" text)))
        ;; Now take care of subfigures.
        (while (search-forward "\\begin{figure}" nil t)
          (narrow-to-region
           (point-at-bol) (save-excursion (search-forward "\\end{figure}")))
          (when (search-forward "\\begin{subfigure}" nil t)
            (beginning-of-line)
            (insert "\\makebox[\\textwidth]{\n")
            (while (search-forward "\\end{subfigure}" nil t))
            (end-of-line)
            (insert "}"))
          (widen))
        (buffer-string)))))

(add-to-list 'org-export-filter-body-functions
             'pank/org-latex-wrap-figures-in-makebox)


(defun pank/org-latex-wrap-oversized-tabulars-in-makebox (table backend info)
  "If the width is larger than \\textwidth center LaTeX tabulars."
  (when (org-export-derived-backend-p backend 'latex )
    (with-temp-buffer
      (save-excursion (insert table))
      (let ((env (save-excursion (and (search-forward-regexp "\\\\begin{\\(tabular[^}]*?\\)}" nil t)
                                      (match-string 1))))
            (makeboxedp (search-forward "\\makebox" (match-beginning 0) t))
            (width (save-excursion
                     (cl-loop while (not (eobp)) do
                              (forward-line)
                              maximize
                              (if (looking-at "[ \t]*\\\\") 0
                                (- (line-end-position) (line-beginning-position)))))))
        (when (and env (not makeboxedp) (> width 100))
          (goto-char (point-min))
          (when (search-forward env)
            (beginning-of-line)
            (insert "\\makebox[\\textwidth]{%\n")
            (search-forward (format "\\end{%s}" env))
            (insert "}")
            (buffer-string)))))))

(mapc (lambda (elm)
        (add-to-list elm 'pank/org-latex-wrap-oversized-tabulars-in-makebox))
      '(org-export-filter-table-functions
        org-export-filter-export-block-functions
        org-export-filter-latex-environment-functions
        org-export-filter-special-block-functions))


(defun pank/org-latex-table-and-figures (body backend info)
  "Make sure that figure~\\ref has nobreak-space"
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string
     "\\([fF]igure\\|[tT]able\\|[eE]quation\\) +\\(\\\\ref\\|eqref\\)"
     "\\1~\\2" body t)))

(add-to-list 'org-export-filter-body-functions
             'pank/org-latex-table-and-figures)


;;* dynamic blocks

(defun org-dblock-write:thanks (params)
  "Reads THANKS file in same folder and format.
PARAMS can hold :filename, :keyword, and :prefix.
The content of the block will be inserted as the postfix."
  (when (file-exists-p (or (plist-get params :filename) "THANKS"))
    (insert
     (with-temp-buffer
       (save-excursion
         (insert (plist-get params :prefix) " "
                 (replace-regexp-in-string
                  ".*?\\(,\\)[^,]*\\'" ", and"
                  (mapconcat
                   'identity
                   (remove-if
                    (lambda (str) (string-match-p "^\\s-*$" str))
                    (split-string
                     (with-temp-buffer (save-excursion
                                         (insert-file-contents "THANKS"))
                                       (flush-lines  "^\\s-*$" (point-min) (point-max))
                                       (buffer-string)) "\\s-*\n+\\s-*"))
                   ", ")
                  nil nil 1) ".\n"
                  (or (plist-get params :content) "")))
       (while (not (eobp))
         (beginning-of-line)
         (insert "#+thanks: ")
         (end-of-line)
         (let ((next-line-add-newlines nil)) (next-line +1)))
       (beginning-of-buffer)
       (flush-lines "^#\\+thanks:[ \t]+$")
       (buffer-string)))))

(add-hook 'org-export-before-processing-hook
          (lambda (backend) (org-dblock-update t)))

;;* prepare hooks
(defun pank/add-subtitle (backend)
  (interactive)
  (when (and (org-export-derived-backend-p backend 'latex)
             (not (version< "8.2" (org-version))))
    (goto-char (point-min))
    (when (search-forward-regexp "^#\\+SUBTITLE:[ \t]*\\(.*\\)$" nil t)
      (replace-match "#+LATEX_HEADER: \\\\subtitle{\\1}"))))

(defun pank/author-fix (backend)
  (interactive)
  (when (eq backend 'latex)
    (goto-char (point-min))
    (let ((affil
           (save-excursion
             (when (search-forward-regexp "^#\\+AFFILIATION:\\(.*\\)$" nil t)
               (match-string 1)))))
      (when (search-forward "#+AUTHOR:" nil t)
        (while (search-forward "&" (line-end-position) t)
          (replace-match "@@latex:\\\\qquad@@"))
        (when affil
          (end-of-line)
          (insert "@@latex:\\\\[.25em]@@" (org-trim affil)))))))

(defun pank/add-thanks (backend)
  (interactive)
  (require 'cl)
  (when (eq backend 'latex)
    (goto-char (point-min))
    (let ((thanks
	   (save-excursion
             (mapconcat 'identity
                        (cl-loop while (search-forward-regexp "#\\+THANKS:\\(.*\\)$" nil t)
                                 collect (org-trim (match-string 1))) " "))))
      (when (and (org-string-nw-p thanks)
                 (search-forward "#+TITLE:" nil t))
        (end-of-line)
        (insert (format "@@latex:\\thanks{@@%s@@latex:}@@"
                        (org-trim thanks)))))))

(defun pank/odt-fix-math (backend)
  "odt doesn't work with x^*"
  (interactive)
  (when (org-export-derived-backend-p backend 'odt)
    (goto-char 0)
    (save-excursion
      (while (search-forward-regexp "\\(\\^{? *\\* *}?\\)" nil t)
        (when (memq (save-match-data (org-element-type (org-element-context)))
                    '(latex-math latex-fragment))
          (replace-match "^{\\\\star}"))))
    (save-excursion
      (while (search-forward "\\mathbb" nil t)
        (replace-match "\\\\mathbf")))))

(defun pank/odt-fix-text (backend)
  (interactive)
  (when (org-export-derived-backend-p backend 'odt)
    (while (search-forward-regexp
            "#\\+\\(?:begin\\|end\\)_\\(abstract\\)" nil t)
      (replace-match "quote" t nil nil 1))))

(mapc (lambda (hook) (add-hook 'org-export-before-processing-hook hook))
      '(pank/add-subtitle pank/add-thanks pank/author-fix pank/odt-fix-math pank/odt-fix-text))

;; math in odt

;; (setq org-latex-to-mathml-convert-command
;;       ;; "latexml --destination=%o %I "
;;       "java -jar %j -unicode -force -df %o %I"
;;       org-latex-to-mathml-jar-file
;;       "~/bin/mathtoweb.jar")

(setq org-latex-to-mathml-convert-command
      "latexmlmath \"%i\" --presentationmathml=%o")

;; don't ask, always steal file.
(defun ask-user-about-lock (file opponent) "Always steal lock." t)
